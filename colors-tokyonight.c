static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#15161e", /* black   */
  [1] = "#f7768e", /* red     */
  [2] = "#9ece6a", /* green   */
  [3] = "#e0af68", /* yellow  */
  [4] = "#7aa2f7", /* blue    */
  [5] = "#bb9af7", /* magenta */
  [6] = "#7dcfff", /* cyan    */
  [7] = "#a9b1d6", /* white   */

  /* 8 bright colors */
  [8]  = "#414868", /* black   */
  [9]  = "#ff9aad", /* red     */
  [10] = "#bae68b", /* green   */
  [11] = "#eec68c", /* yellow  */
  [12] = "#9dbdff", /* blue    */
  [13] = "#c8acfc", /* magenta */
  [14] = "#a3d4f0", /* cyan    */
  [15] = "#c0caf5", /* white   */

  /* special colors */
  [256] = "#1a1b26", /* background */
  [257] = "#c0caf5", /* foreground */
};
