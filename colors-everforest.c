static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#7a8478", /* black   */
  [1] = "#e67e80", /* red     */
  [2] = "#a7c080", /* green   */
  [3] = "#dbbc7f", /* yellow  */
  [4] = "#7fbbb3", /* blue    */
  [5] = "#d699b6", /* magenta */
  [6] = "#83c092", /* cyan    */
  [7] = "#d3c6aa", /* white   */

  /* 8 bright colors */
  [8]  = "#7a8478", /* black   */
  [9]  = "#e67e80", /* red     */
  [10] = "#a7c080", /* green   */
  [11] = "#dbbc7f", /* yellow  */
  [12] = "#7fbbb3", /* blue    */
  [13] = "#d699b6", /* magenta */
  [14] = "#83c092", /* cyan    */
  [15] = "#d3c6aa", /* white   */

  /* special colors */
  [256] = "#232a2e", /* background */
  [257] = "#d3c6aa", /* foreground */
};
