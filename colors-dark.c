static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#252a34", /* black   */
  [1] = "#e06c8a", /* red     */
  [2] = "#79c38f", /* green   */
  [3] = "#d1b166", /* yellow  */
  [4] = "#71a9ec", /* blue    */
  [5] = "#de8ede", /* magenta */
  [6] = "#78c5ca", /* cyan    */
  [7] = "#abb2bf", /* white   */

  /* 8 bright colors */
  [8]  = "#484f5b", /* black   */
  [9]  = "#e06c8a", /* red     */
  [10] = "#79c38f", /* green   */
  [11] = "#d1b166", /* yellow  */
  [12] = "#71a9ec", /* blue    */
  [13] = "#de8ede", /* magenta */
  [14] = "#78c5ca", /* cyan    */
  [15] = "#ffffff", /* white   */

  /* special colors */
  [256] = "#16181a", /* background */
  [257] = "#abb2bf", /* foreground */
};
