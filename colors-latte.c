static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#5c5f77", /* black   */
  [1] = "#d20f39", /* red     */
  [2] = "#40a02b", /* green   */
  [3] = "#fe640b", /* yellow  */
  [4] = "#1e66f5", /* blue    */
  [5] = "#8839ef", /* magenta */
  [6] = "#179299", /* cyan    */
  [7] = "#7c7f93", /* white   */

  /* 8 bright colors */
  [8]  = "#6c6f85", /* black   */
  [9]  = "#e64553", /* red     */
  [10] = "#60c04b", /* green   */
  [11] = "#df8e1d", /* yellow  */
  [12] = "#04a5e5", /* blue    */
  [13] = "#ea76cb", /* magenta */
  [14] = "#209fb5", /* cyan    */
  [15] = "#acb0be", /* white   */

  /* special colors */
  [256] = "#eff1f5", /* background */
  [257] = "#4c4f69", /* foreground */
};
