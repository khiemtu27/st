static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#090618", /* black   */
  [1] = "#c34043", /* red     */
  [2] = "#76946a", /* green   */
  [3] = "#c0a36e", /* yellow  */
  [4] = "#7e9cd8", /* blue    */
  [5] = "#957fb8", /* magenta */
  [6] = "#6a9589", /* cyan    */
  [7] = "#c8c093", /* white   */

  /* 8 bright colors */
  [8]  = "#727169", /* black   */
  [9]  = "#e82424", /* red     */
  [10] = "#98bb6c", /* green   */
  [11] = "#e6c384", /* yellow  */
  [12] = "#7fb4ca", /* blue    */
  [13] = "#938aa9", /* magenta */
  [14] = "#7aa89f", /* cyan    */
  [15] = "#dcd7ba", /* white   */

  /* special colors */
  [256] = "#1f1f28", /* background */
  [257] = "#dcd7ba", /* foreground */
};
