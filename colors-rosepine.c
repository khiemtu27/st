static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#403d52", /* black   */
  [1] = "#eb6f92", /* red     */
  [2] = "#f6c177", /* green   */
  [3] = "#ebbcba", /* yellow  */
  [4] = "#31748f", /* blue    */
  [5] = "#c4a7e7", /* magenta */
  [6] = "#9ccfd8", /* cyan    */
  [7] = "#6e6a86", /* white   */

  /* 8 bright colors */
  [8]  = "#524f67", /* black   */
  [9]  = "#eb6f92", /* red     */
  [10] = "#f6c177", /* green   */
  [11] = "#ebbcba", /* yellow  */
  [12] = "#31748f", /* blue    */
  [13] = "#c4a7e7", /* magenta */
  [14] = "#9ccfd8", /* cyan    */
  [15] = "#908caa", /* white   */

  /* special colors */
  [256] = "#191724", /* background */
  [257] = "#e0def4", /* foreground */
};
