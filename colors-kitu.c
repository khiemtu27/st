static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#2c3223", /* black   */
  [1] = "#a55d50", /* red     */
  [2] = "#858a44", /* green   */
  [3] = "#9f6e4e", /* yellow  */
  [4] = "#5f929b", /* blue    */
  [5] = "#8f83ad", /* magenta */
  [6] = "#70a39b", /* cyan    */
  [7] = "#d0d0ab", /* white   */

  /* 8 bright colors */
  [8]  = "#3b4233", /* black   */
  [9]  = "#be6363", /* red     */
  [10] = "#c5cd79", /* green   */
  [11] = "#ce755a", /* yellow  */
  [12] = "#73c7d3", /* blue    */
  [13] = "#c0b0e4", /* magenta */
  [14] = "#9fe1ce", /* cyan    */
  [15] = "#eae7ca", /* white   */

  /* special colors */
  [256] = "#1f2416", /* background */
  [257] = "#bbbb9b", /* foreground */
};
