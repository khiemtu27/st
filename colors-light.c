static const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#696e76", /* black   */
  [1] = "#dc5b5b", /* red     */
  [2] = "#67c059", /* green   */
  [3] = "#d48a56", /* yellow  */
  [4] = "#6892c0", /* blue    */
  [5] = "#dc8da8", /* magenta */
  [6] = "#66c7b0", /* cyan    */
  [7] = "#b2b7bb", /* white   */

  /* 8 bright colors */
  [8]  = "#79828d", /* black   */
  [9]  = "#d59090", /* red     */
  [10] = "#97ba92", /* green   */
  [11] = "#c9b285", /* yellow  */
  [12] = "#8ca5b9", /* blue    */
  [13] = "#d0a1c5", /* magenta */
  [14] = "#a1cac3", /* cyan    */
  [15] = "#d5dbde", /* white   */

  /* special colors */
  [256] = "#eef3f6", /* background */
  [257] = "#4f555a", /* foreground */
};
